import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { LoginService } from './login.service';
import { first } from "rxjs/operators";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginform = this.fb.group({
    
    email: ['', Validators.required ],
    password: ['', Validators.required],

    
  });
  constructor(public fb:FormBuilder,private toastr: ToastrService,private router: Router,private loginservice: LoginService) { }

  ngOnInit() {
    // if(!window.localStorage.getItem('token')) {
    //   this.router.navigate(['login']);
    //   return;
    // }
    // this.loginservice.getUserById()
    //   .subscribe( data => {
    //       this.users = data.result;
    //   });
  }
  email = new FormControl('');
  password = new FormControl('');
  login(){
    if(this.loginform.valid){
      this.loginservice.adminlogin(this.loginform.value)
      .subscribe( data => {
        console.log(data);
        this.toastr.success("Login Sucessfully")
        this.router.navigate(['/profile']);
      });

    }
else{
  console.log("error");
}  
}
}
