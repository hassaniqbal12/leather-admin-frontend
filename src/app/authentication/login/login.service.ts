import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserModel} from './usermodel';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  baseurl: string = "http://localhost:3000/api/admins/";
  constructor(private http:HttpClient) { }
  adminlogin(admins: UserModel){
    return this.http.post(this.baseurl + 'login', admins);
  }
  // getUserById(id: number) {
  //   return this.http.get(this.baseurl + id);
  // }
}
