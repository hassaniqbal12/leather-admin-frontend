import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserModel} from './usermodel';
@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  baseurl: string = "http://localhost:3000/api/";
  constructor(private http:HttpClient) { }
  adduser(user: UserModel){
    return this.http.post(this.baseurl + 'rusers', user);
  }
}
