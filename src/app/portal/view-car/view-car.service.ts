import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ViewCarService {

  baseurl: string = "http://localhost:3000/api/";
  constructor(private http:HttpClient) { }
  viewallcars(){
    return this.http.get(this.baseurl + 'cars');
  }
}
