export class ViewcarModel {
    id: string;
    name: String;
    model: String;
    driven: String;
    licenseno: number;
    carimage: string;
    created_at: Date;
    updated_at: Date;
}