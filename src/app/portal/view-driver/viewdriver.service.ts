import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ViewdriverService {

  baseurl: string = "http://localhost:3000/api/";
  constructor(private http:HttpClient) { }
  viewalldriver(){
    return this.http.get(this.baseurl + 'drivers');
  }
}
