import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { ProfileService } from './profile.service';
import { first } from "rxjs/operators";
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(public fb:FormBuilder,private toastr: ToastrService,private router: Router,private profileservice: ProfileService) { }

  ngOnInit() {
  }
  loguot(){ 
      this.profileservice.logout;
        this.toastr.success("Logout Sucessfully")
        this.router.navigate(['/login']);
  }
}
