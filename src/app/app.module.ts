import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';  
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './authentication/register/register.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { LoginComponent } from './authentication/login/login.component';
import { ProfileComponent } from './portal/profile/profile.component';
import { AddCarComponent } from './portal/add-car/add-car.component';
import { ViewCarComponent } from './portal/view-car/view-car.component';
import { AddDriverComponent } from './portal/add-driver/add-driver.component';
import { ViewDriverComponent } from './portal/view-driver/view-driver.component';
import { ForgetPasswordComponent } from './authentication/forget-password/forget-password.component';
import { ForgetEmailComponent } from './authentication/forget-email/forget-email.component';
import { ProductsComponent } from './portal/products/products.component';
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    PagenotfoundComponent,
    LoginComponent,
    ProfileComponent,
    AddCarComponent,
    ViewCarComponent,
    AddDriverComponent,
    ViewDriverComponent,
    ForgetPasswordComponent,
    ForgetEmailComponent,
    ProductsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,  
     FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    HttpClientModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
